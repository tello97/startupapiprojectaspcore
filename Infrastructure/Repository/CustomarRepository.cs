﻿using Core.Entities;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Repository
{
    public class CustomarRepository : Repository<Customar>, ICustomarRepository
    {
        private readonly ApplicationDbContext _context;
        public CustomarRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        public IEnumerable<Customar> GetBestCustomars(DateTime BirtDate) => _context.Customars.Where(x => x.BirthDate < BirtDate).ToList();

    }
}
