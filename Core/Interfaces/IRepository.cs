﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Core.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();

        T GetById(object Id);

        void Add(T Item);

        IEnumerable<T> Find(Expression<Func<T, bool>> expression);

        void AddRange(IEnumerable<T> Items);

        void Update(T Item);

        void Remove(T Item);

        void RemoveRange(IEnumerable<T> Items);
    }
}
