﻿using Core.Entities;
using System;
using System.Collections.Generic;

namespace Core.Interfaces
{
    public interface ICustomarRepository : IRepository<Customar>
    {
        IEnumerable<Customar> GetBestCustomars(DateTime BirtDate);
    }
}
