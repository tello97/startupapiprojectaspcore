﻿using Core.Interfaces;
using Infrastructure.Repository;

namespace Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        public ICustomarRepository Customar => new CustomarRepository(_context);

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
