﻿using Core.Entities;
using FluentValidation;

namespace WebApi.Validations
{
    public class CustomarValidator : AbstractValidator<Customar>
    {
        public CustomarValidator()
        {
            RuleFor(x => x.FullName).Length(4);
            RuleFor(x => x.FullName).NotNull();
            RuleFor(x => x.FullName).Matches("s[a-z]");

        }
    }
}
