﻿using AutoMapper;
using Core.Entities;
using StartupApiProject.ViewModels;

namespace StartupApiProject.MappingProfiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customar, CustomarViewModel>();
        }
    }
}
