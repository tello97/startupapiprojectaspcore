﻿using System;

namespace Core.Entities
{
    public class Customar : EntityBase
    {
        public string FullName { get; set; }

        public DateTime BirthDate { get; set; }

        public Address Address { get; set; }
    }
}
