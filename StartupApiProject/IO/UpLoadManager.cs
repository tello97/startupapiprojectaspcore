﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace WebApi.IO
{
    public class UpLoadManager
    {
        private readonly IHostingEnvironment hosting;
        private string Path = "";

        public UpLoadManager(IHostingEnvironment hosting)
        {
            this.hosting = hosting;
            Path = hosting.WebRootPath + "\\UpLoad\\";
        }

        public string UpLoad(IFormFile file)
        {
            try
            {
                if (file.Length > 0)
                {
                    if (!Directory.Exists(Path))
                        Directory.CreateDirectory(Path);
                    using (FileStream fileStream = System.IO.File.Create(Path + file.FileName))
                    {
                        file.CopyTo(fileStream);
                        fileStream.Flush();
                        return Path + file.FileName;
                    }

                }
                else return "Failed";
            }
            catch (System.Exception ex)
            {
                return ex.Message.ToString();
            }

        }
    }
}
