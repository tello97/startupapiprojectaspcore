﻿using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StartupApiProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApi.IO;

namespace StartupApiProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomarsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _Mapper;
        private readonly UpLoadManager upLoadManager;
        private readonly DownLoadManager downLoadManager;

        public CustomarsController(IUnitOfWork unitOfWork, IMapper _mapper, UpLoadManager upLoadManager, DownLoadManager downLoadManager)
        {
            _unitOfWork = unitOfWork;
            _Mapper = _mapper;
            this.upLoadManager = upLoadManager;
            this.downLoadManager = downLoadManager;
        }

        // GET api/Customars
        [HttpGet]
        public ActionResult<IEnumerable<Customar>> Get()
        {
            return Ok(_Mapper.Map<List<Customar>, List<CustomarViewModel>>(_unitOfWork.Customar.GetAll().ToList()));
        }

        [HttpGet("GetBest")]
        public ActionResult<IEnumerable<Customar>> GetBest()
        {
            return Ok(_unitOfWork.Customar.GetBestCustomars(DateTime.Now));
        }

        // GET api/Customars/5
        [HttpGet("{id}")]
        public ActionResult<Customar> Get(int id)
        {
            return Ok(_unitOfWork.Customar.GetById(id));
        }

        // POST api/Customars
        [HttpPost]
        public IActionResult Post(Customar item)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.ErrorCount);
            _unitOfWork.Customar.Add(item);
            _unitOfWork.Save();
            return Ok();
        }

        [HttpPost("UpLoadCustomarImage")]
        public void UpLoadCustomarImage(IFormFile file)
        {
            upLoadManager.UpLoad(file);
        }


        // PUT api/Customars
        [HttpPut]
        public void Put(Customar item)
        {
            _unitOfWork.Customar.Update(item);
            _unitOfWork.Save();
        }

        // DELETE api/Customars/5
        [HttpDelete("{id}")]
        public void Delete(Customar item)
        {
            _unitOfWork.Customar.Remove(item);
            _unitOfWork.Save();
        }













        [HttpGet("DownLoadCustomarImage")]

        public IActionResult DownloadFile(string filename)
        {
            return downLoadManager.GetFile(filename);
        }


    }
}