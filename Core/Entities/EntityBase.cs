﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.Now;

        public bool IsDeleted { get; set; }
    }
}
