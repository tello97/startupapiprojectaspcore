﻿namespace Core.Interfaces
{
    public interface IUnitOfWork
    {
        ICustomarRepository Customar { get; }

        void Save();
    }
}
