﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System.IO;

namespace WebApi.IO
{
    public class DownLoadManager
    {
        private readonly IHostingEnvironment hosting;
        private string Path = "";

        public DownLoadManager(IHostingEnvironment hosting)
        {
            this.hosting = hosting;
            Path = hosting.WebRootPath + "\\UpLoad\\";
        }

        private string GetMimeType(string fileName)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;
            if (!provider.TryGetContentType(fileName, out contentType))
            {
                contentType = "application/octet-stream";
            }
            return contentType;
        }

        public FileContentResult GetFile(string filename)
        {
            var filepath = Path + filename; ;

            var mimeType = this.GetMimeType(filename);

            byte[] fileBytes = null;

            if (File.Exists(filepath))
            {
                fileBytes = File.ReadAllBytes(filepath);
            }
            else
            {
                throw new IOException("file not Exists");
            }

            return new FileContentResult(fileBytes, mimeType)
            {
                FileDownloadName = filename
            };
        }
    }
}
