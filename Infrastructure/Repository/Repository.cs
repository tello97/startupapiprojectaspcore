﻿using Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Infrastructure.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly ApplicationDbContext _context;
        private DbSet<T> table;

        public Repository(ApplicationDbContext dbContext)
        {
            _context = dbContext;
            table = _context.Set<T>();
        }

        public void Add(T Item)
        {
            table.AddAsync(Item);
        }

        public void AddRange(IEnumerable<T> Items)
        {
            table.AddRangeAsync(Items);
        }


        public IEnumerable<T> Find(Expression<Func<T, bool>> expression) => table.Where(expression).ToList();


        public IEnumerable<T> GetAll() => table.ToList();

        public T GetById(object Id) => table.Find(Id);

        public void Remove(T Item)
        {
            table.Remove(Item);
        }

        public void RemoveRange(IEnumerable<T> Items)
        {
            table.RemoveRange(Items);
        }

        public void Update(T item)
        {
            table.Attach(item);
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
